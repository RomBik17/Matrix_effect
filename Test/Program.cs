﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading;

namespace Test
{
    class Program
    {
        class Matrix
        {
            private List<StringBuilder> chain = new List<StringBuilder>();
            private int topLine;
            private int max_lendth;
            private const int max_position = 29;
            public Matrix()
            {
                StringBuilder str = new StringBuilder("");
                Random rand = new Random();
                topLine = rand.Next(0, 20);
                max_lendth = max_position - topLine;
                int length = rand.Next(3, 10);
                for (int i = 0; i < length; i++)
                {
                    char tmp = (char)rand.Next('A', 'Z');
                    str.Append(tmp);
                }
                chain.Add(str);
            }
            protected void randomize(ref StringBuilder str)
            {
                Random rand = new Random();
                int length = rand.Next(3, 10);
                for (int i = 0; i < length; i++)
                {
                    char tmp = (char)rand.Next('A', 'Z');
                    str.Append(tmp);
                }
            }
            protected void build()
            {
                if (chain.Count < max_lendth)
                {
                    StringBuilder str = new StringBuilder("");
                    randomize(ref str);
                    chain.Add(str);
                }
                else
                {
                    chain.Add(chain[0]);
                    chain.RemoveAt(0);
                }
            }
            protected void output()
            {
                Console.Clear();
                Console.SetCursorPosition(0, topLine);
                foreach (var littleChain in chain)
                {
                    Console.Write(littleChain[0]);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(littleChain[1]);
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    for(int j = 2; j < littleChain.Length; j++)
                    {
                        Console.Write(littleChain[j]);
                    }
                    Console.WriteLine();
                    Console.ResetColor();
                }
                Thread.Sleep(75);
            }
            public void work()
            {
                for(int i = 0; i < chain.Count; i++)
                {
                    StringBuilder str = new StringBuilder("");
                    randomize(ref str);
                    chain[i] = str; 
                }
                this.output();
                this.build();
            }
            ~Matrix()
            {
                chain.Clear();
            }
        }
        static void Main(string[] args)
        {
            Matrix mt = new Matrix();
            while (true)
            {
                mt.work();
                if (Console.KeyAvailable) break;
            }
        }
    }
}